#pragma once
#include <spring\Framework\IScene.h>
#include <qcustomplot.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
		
	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QPushButton *pushButtonStop;
		QPushButton *pushButtonBack;
		QPushButton *pushButtonStart;
		QSpacerItem *horizontalSpacer;
		QCustomPlot *customPlot;

	private slots:
		void buttonBack();
	};

}
