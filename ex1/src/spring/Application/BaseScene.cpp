#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{

		createGUI();

		QObject::connect(pushButtonBack, SIGNAL(released()), this, SLOT(buttonBack()));
		
	}

	void BaseScene::release()
	{
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		pushButtonStop = new QPushButton(centralWidget);
		pushButtonStop->setObjectName(QStringLiteral("pushButtonStop"));

		gridLayout->addWidget(pushButtonStop, 1, 2, 1, 1);

		pushButtonBack = new QPushButton(centralWidget);
		pushButtonBack->setObjectName(QStringLiteral("pushButtonBack"));
		pushButtonBack->setShortcut(QStringLiteral("Shift+B"));

		gridLayout->addWidget(pushButtonBack, 1, 3, 1, 1);

		pushButtonStart = new QPushButton(centralWidget);
		pushButtonStart->setObjectName(QStringLiteral("pushButtonStart"));
		pushButtonStart->setShortcut(QStringLiteral("Shift+S"));

		gridLayout->addWidget(pushButtonStart, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));

		gridLayout->addWidget(customPlot, 0, 0, 1, 4);

		m_uMainWindow.get()->setCentralWidget(centralWidget);

		m_uMainWindow.get()->setWindowTitle("DisplayWindow");
		pushButtonStop->setText("Sto&p");
		pushButtonStop->setShortcut(QApplication::translate("MainWindow", "Alt+P", Q_NULLPTR));
		pushButtonBack->setText("&Back");
		pushButtonBack->setShortcut(QApplication::translate("MainWindow", "Alt+B", Q_NULLPTR));
		pushButtonStart->setText("&Start");
		pushButtonStart->setShortcut(QApplication::translate("MainWindow", "Alt+S", Q_NULLPTR));
	}
	void BaseScene::buttonBack()
	{

		std::string title = "DisplayWindow";
		m_uMainWindow->setWindowTitle(QString(title.c_str()));

		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
