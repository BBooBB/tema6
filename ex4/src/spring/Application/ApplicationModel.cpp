#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <iostream>
#include <fstream>
const std::string initialSceneName = "InitialScene";
const std::string baseSceneName = "BaseScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(baseSceneName, baseScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 
		std::ifstream f("C:/Users/Teodora/Desktop/Siemenss/Tema6/tema6/ex4/src/spring/Application/date.txt");	//nu mi-a mers cu cale absoluta

		std::string applicationName,applicationNameKey; 
		f >> applicationName;
		f >> applicationNameKey;
		m_TransientData.emplace(applicationNameKey, applicationName);

		unsigned int sampleRate;
		std::string sampleRateKey;
		f >> sampleRate;
		f >> sampleRateKey;
		m_TransientData.emplace(sampleRateKey, sampleRate);

		double displayTime;
		std::string displayTimeKey;
		f >> displayTime;
		f >> displayTimeKey;
		m_TransientData.emplace(displayTimeKey, displayTime);

		unsigned int refreshRate;
		std::string refreshRateKey;
		f >> refreshRate;
		f >> refreshRateKey;
		m_TransientData.emplace(refreshRateKey, refreshRate);
	}
}
