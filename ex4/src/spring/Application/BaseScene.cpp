#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		createGUI();
	}

	void BaseScene::release()
	{
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		pushButtonStart = new QPushButton(centralWidget);
		pushButtonStart->setObjectName(QStringLiteral("pushButtonStart"));

		gridLayout->addWidget(pushButtonStart, 1, 1, 1, 1);

		pushButtonStop = new QPushButton(centralWidget);
		pushButtonStop->setObjectName(QStringLiteral("pushButtonStop"));

		gridLayout->addWidget(pushButtonStop, 1, 2, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));
		gridLayout->addWidget(customPlot, 0, 0, 1, 3);


		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);

		m_uMainWindow.get()->setWindowTitle("DisplayWindow");
		pushButtonStart->setText("Start");
		pushButtonStop->setText("Stop");
	}
}
