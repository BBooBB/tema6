#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(okButton, SIGNAL(released()), this, SLOT(mf_OkButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(541, 285);

		centralWidget = new QWidget(m_uMainWindow.get());

		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);

		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);

		sampleRateSpinBox = new QSpinBox(centralWidget);
		sampleRateSpinBox->setMaximum(25600);
		unsigned int defaultSampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("SampleRate")->second);
		sampleRateSpinBox->setValue(defaultSampleRate);
		gridLayout->addWidget(sampleRateSpinBox, 3, 2, 1, 1);

		applicationNameLineEdit = new QLineEdit(centralWidget);
		std::string defaultAppName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);
		applicationNameLineEdit->setText(QString(defaultAppName.c_str()));
		gridLayout->addWidget(applicationNameLineEdit, 1, 2, 1, 1);

		applicationNameLabel = new QLabel(centralWidget);
		gridLayout->addWidget(applicationNameLabel, 1, 1, 1, 1);
		applicationNameLabel->setText("Application Name");

		refreshRatespinBox = new QSpinBox(centralWidget);
		boost::any testValue = m_TransientDataCollection.find("RefreshRate")->second;
		if (testValue.type() == typeid (unsigned int))
		{
			std::cout << "integer, as expected";
		}
		unsigned int defaultRefreshRate = boost::any_cast<unsigned int>(m_TransientDataCollection.find("RefreshRate")->second);
		refreshRatespinBox->setValue(defaultRefreshRate);
		gridLayout->addWidget(refreshRatespinBox, 5, 2, 1, 1);

		sampleRateLabel = new QLabel(centralWidget);
		gridLayout->addWidget(sampleRateLabel, 3, 1, 1, 1);
		sampleRateLabel->setText("Sample Rate");


		topVerticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(topVerticalSpacer, 0, 2, 1, 1);

		bottomVerticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(bottomVerticalSpacer, 6, 2, 1, 1);

		displayTimeLabel = new QLabel(centralWidget);
		gridLayout->addWidget(displayTimeLabel, 4, 1, 1, 1);
		displayTimeLabel->setText("Display Time");

		refreshRateLabel = new QLabel(centralWidget);
		gridLayout->addWidget(refreshRateLabel, 5, 1, 1, 1);
		refreshRateLabel->setText("Refresh Rate");

		leftHorizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(leftHorizontalSpacer, 3, 0, 1, 1);

		rightHorizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(rightHorizontalSpacer, 3, 3, 1, 1);

		okButton = new QPushButton(centralWidget);
		gridLayout->addWidget(okButton, 6, 3, 1, 1);
		okButton->setText("OK");

		displayTimeDoubleSpinBox = new QDoubleSpinBox(centralWidget);
		displayTimeDoubleSpinBox->setMinimum(0);
		displayTimeDoubleSpinBox->setSingleStep(0.1);
		double defaultDisplayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);
		displayTimeDoubleSpinBox->setValue(defaultDisplayTime);
		gridLayout->addWidget(displayTimeDoubleSpinBox, 4, 2, 1, 1);

		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);
	}

	void InitialScene::mf_OkButton()
	{
		const std::string appName = applicationNameLineEdit->text().toStdString();

		if (!appName.empty())
		{
			m_TransientDataCollection.erase("ApplicationName");
			m_TransientDataCollection.emplace("ApplicationName", appName);
		}

		const unsigned int newSampleRate = sampleRateSpinBox->value();
		m_TransientDataCollection["SampleRate"] = newSampleRate;
		

		double newDisplayTime = displayTimeDoubleSpinBox->value();
		m_TransientDataCollection["DisplayTime"] = displayTimeDoubleSpinBox;
	
		unsigned int newRefreshRate = refreshRatespinBox->value();
		m_TransientDataCollection["RefreshRate"] = newRefreshRate;
		

		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);
	}

	
}

