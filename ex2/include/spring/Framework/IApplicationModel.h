#pragma once
#include <Spring/Framework/IScene.h>
#include <Spring/Framework/global.h>

namespace Spring
{

	class Framework_EXPORT_IMPORT_API IApplicationModel
	{
	public:

		virtual void defineScene() = 0;

		virtual void defineInitialScene() = 0;

		virtual void defineTransientData() = 0;

		virtual const std::string& sGetInitialSceneName()
		{
			return mv_szInitialScene;
		}

		std::map< std::string, IScene*> getScenes() 
		{
			return m_Scenes;
		}

		std::map< std::string, boost::any> getTransientData() 
		{
			return m_TransientData;
		}

		IScene* getInitialScene() 
		{
			return m_Scenes.find(mv_szInitialScene)->second;
		}

		virtual ~IApplicationModel()
		{
			;
		}

	protected:
		std::string mv_szInitialScene;

		std::map< std::string, IScene*> m_Scenes;
		std::map< std::string, boost::any> m_TransientData;
	};

}
