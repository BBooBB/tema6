#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>
namespace Spring
{
	class BaseScene : public IScene
	{

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		
	private:
		void createGUI();
		QCustomPlot *customPlot;
		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QPushButton *pushButtonStart;
		QPushButton *pushButtonStop;
		QSpacerItem *horizontalSpacer;
		QListView *listView;
		QMenuBar *menuBar;
		~BaseScene();
		
		
	};

}
