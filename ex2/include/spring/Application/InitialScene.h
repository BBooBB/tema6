#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		/*template<typename T>
		  boost::any operator =(T a);*/

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QSpinBox *sampleRateSpinBox;
		QLineEdit *applicationNameLineEdit;
		QLabel *applicationNameLabel;
		QSpinBox *refreshRatespinBox;
		QLabel *sampleRateLabel;
		QSpacerItem *topVerticalSpacer;
		QSpacerItem *bottomVerticalSpacer;
		QLabel *displayTimeLabel;
		QLabel *refreshRateLabel;
		QSpacerItem *leftHorizontalSpacer;
		QSpacerItem *rightHorizontalSpacer;
		QPushButton *okButton;
		QDoubleSpinBox *displayTimeDoubleSpinBox;
		QToolBar *mainToolBar;
		QStatusBar *statusBar;

		private slots:
		void mf_OkButton();

	};
}
