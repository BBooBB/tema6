#pragma once

#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

namespace Spring
{
	class TrivialScene :public IScene
	{
		Q_OBJECT
	public:
		TrivialScene(const std::string &, int, double);
		~TrivialScene();

		void createScene() override;

		void release() override;

		double nrOfSamples();
	private:
		QWidget *centralWidget;
		QPushButton *showButton;
		QPushButton *backButton;

		void createGUI();

		int sampleRate;
		double displayTime;

	private slots:
		void back_Button();
		void show_Button();
	};
}

