#include <..\..\..\include\spring\Application\TrivialScene.h>
namespace Spring
{

	TrivialScene::TrivialScene(const std::string & sceneName, int sampleRate, double displayTime):IScene (sceneName)
	{
		this->sampleRate = sampleRate;
		this->displayTime = displayTime;
	}

	TrivialScene::~TrivialScene()
	{
	}

	void TrivialScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

		createGUI();

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(back_Button()));
		QObject::connect(showButton, SIGNAL(released()), this, SLOT(show_Button()));
	}

	void TrivialScene::createGUI()
	{
		m_uMainWindow.get()->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		showButton = new QPushButton(centralWidget);
		showButton->setObjectName(QStringLiteral("showButton"));
		showButton->setEnabled(true);
		showButton->setGeometry(QRect(120, 90, 131, 28));
		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));
		backButton->setGeometry(QRect(240, 180, 131, 28));
		m_uMainWindow.get()->setCentralWidget(centralWidget);

		m_uMainWindow.get()->setWindowTitle("MainWindow");
		showButton->setText("ShowNrOfSamples");
		backButton->setText("Back");
	}

	void TrivialScene::release()
	{}

	double TrivialScene::nrOfSamples()
	{
		return sampleRate * displayTime;
	}

	void TrivialScene::show_Button()
	{
		QMessageBox *window = new QMessageBox();
		double samples = nrOfSamples();
		std::string message = "Nr. of samples: " + std::to_string(samples);
		window->setText(QString::fromStdString(message));
		window->show();
	}

	void TrivialScene::back_Button()
	{
		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
